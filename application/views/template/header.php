<?php
/**
 * @author T. Werring info@t-werring.nl
 * @project StartYourOwn
 * @created 15-8-14 10:36
 */
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Start your own | Begin it now <?php echo isset($title) ? "| " . $title : ""; ?></title>
    <link rel="stylesheet" href="/static/css/foundation.css" />
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,600,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/static/css/main.css" />

    <script src="/static/js/vendor/modernizr.js"></script>
</head>
<body>

<div class="row">
    <div class="large-12 columns">
        <h1>Start your own</h1>
    </div>
</div>
