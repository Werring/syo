<?php
/**
 * @author T. Werring info@t-werring.nl
 * @project StartYourOwn
 * @created 15-8-14 10:25
 */
?><div class="row">
    <?php foreach($projects as $project){
    $html = <<<HTML
    <div class="columns large-3 medium-4 small-6 block">
        <div class="projectBlockProgress">
            <div class="projectProgressBoundry" style="background-size: %f%%">&euro;%d / &euro;%d</div>
        </div>
        <span class="projectBlockName">%s</span><a href="/project/%d-%s.html"><img class="gradient_box" src="/static/img/gradient.png" style="background-image: url(%s)"></a>
    </div>
HTML;
        printf($html,($project["pfunded"]/$project["ptarget"])*100,$project["pfunded"],$project["ptarget"],$project["ptitle"],$project["pid"],$project["ptitle"],$project["pthumbnail"]);
    }
    ?>
    <div class="columns large-3 medium-4 small-6">
        <a href="/"><img class="gradient_box" src="/static/img/gradient.png" style=""></a>
    </div>
</div>