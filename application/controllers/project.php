<?php
/**
 * @author T. Werring info@t-werring.nl
 * @project StartYourOwn
 * @created 15-8-14 10:15
 */



class Project extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model("projectmodel");

    }

    public function index()
    {
        $data["projects"] = $this->projectmodel->getProjects();
        $data["title"] = "Projects";
        $this->load->view("template/header",$data);
        $this->load->view("projects",$data);
        $this->load->view("template/footer");
    }

    public function projectByID($pid,$ptitle=null){
        $data["project"] = $this->projectmodel->getProjectData($pid,$ptitle);
        $this->load->view("template/header",$data);
        $this->load->view("project_single",$data);
        $this->load->view("template/footer");

    }

    public function projectsByPCategoryID($pcid){
        $data["projects"] = $this->projectmodel->getProjectByPCategoryId($pcid);
        $data["title"] = ucfirst($this->projectmodel->getCategoryNameById($pcid)). " Projects";
        $this->load->view("template/header",$data);
        $this->load->view("projects",$data);
        $this->load->view("template/footer");
    }
} 