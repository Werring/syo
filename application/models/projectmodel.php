<?php
/**
 * @author T. Werring info@t-werring.nl
 * @project StartYourOwn
 * @created 15-8-14 10:09
 */



class ProjectModel extends CI_Model {
    public function __construct(){
        $this->load->database();
        $this->load->helper('url');
    }
    public function getProjects(){
        $SQL = <<<SQL
SELECT project.id as pid, project.title as ptitle, project.description as pdescription, project.target as ptarget, project.funded as pfunded,
pcategory.id as pcid, pcategory.name as pcname, pcategory.description as pcdescription,
user.id as uid, user.name as uname,
poption.svalue AS pthumbnail
FROM project
INNER JOIN project_pcategory
on project.id = project_pcategory.projectid
INNER JOIN pcategory
on project_pcategory.pcategoryid = pcategory.id
LEFT OUTER JOIN poption
on project.id = poption.projectid
INNER JOIN user
on project.ownerid = user.id
WHERE poption.setting = 'thumbnail'
ORDER BY pcid ASC;
SQL;

        $query = $this->db->query($SQL);

        return $query->result_array();
    }

    public function getProjectByPCategoryId($pcid){
        $SQL = <<<SQL
SELECT project.id as pid, project.title as ptitle, project.description as pdescription, project.target as ptarget, project.funded as pfunded, pcategory.id as pcid, pcategory.name as pcname, pcategory.description as pcdescription, user.id as uid, user.name as uname FROM project
INNER JOIN project_pcategory
on project.id = project_pcategory.projectid
INNER JOIN pcategory
on project_pcategory.pcategoryid = pcategory.id
LEFT OUTER JOIN poption
on project.id = poption.projectid
INNER JOIN user
on project.ownerid = user.id
WHERE poption.setting = 'thumbnail' AND
WHERE pcategory.id = ?
ORDER BY pcid ASC;
SQL;

        $query = $this->db->query($SQL,array($pcid));
        return $query->result_array();
    }

    public function getCategoryNameById($pcid){
        $query = $this->db->get_where('pcategory',array("id"=> $pcid));
        $pcategory = $query->row_array();
        return $pcategory["name"];
    }

    public function getProjectData($pid,$ptitle){
        $this->db->select("project.*, user.name as owner");
        $this->db->from("project");
        $this->db->join("user","project.ownerid=user.id");
        $this->db->where("project.id",intval($pid));
        $pData["project"] = $this->db->get()->row_array();
        if(strtolower($pData['project']['title']) != strtolower($ptitle)) {
            redirect("/project/{$pData['project']['id']}-{$pData['project']['title']}"); die();
        }

        $this->db->select("setting, svalue");
        $this->db->from("poption");
        $this->db->where("projectid",intval($pid));
        $pData["setting"] = array();
        foreach ($this->db->get()->result_array() as $row)
        {
            $pData["setting"][$row["setting"]] = $row["svalue"];
        }
        return $pData;
    }
} 